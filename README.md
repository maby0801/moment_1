# DT173G Webbutveckling III
## Moment 1 - Versionshantering & Git
Författare: *Mattias Bygdeson*

Den här uppgiften har gått ut på att testköra Git. Versionshantering är väldigt bra då det hjälper till med att...

* hålla koll på vilka förändringar som har gjorts, när de har gjorts och vem som har gjort dem
* backa tillbaka och fixa till eventuella fel som uppstår
* låta flera personer jobba på samma projekt samtidigt

För att klona detta projekt från remote repo till local repo så kör man följande kommando:

>`git clone https://maby0801@bitbucket.org/maby0801/moment_1.git`


![Mittuniversitetet logotyp](https://www.miun.se/imagevault/publishedmedia/8zx9t1b34hr3oh6xf6hb/mittuniversitetet_logo.png "Mittuniversitetet")